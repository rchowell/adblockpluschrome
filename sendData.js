/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @module sendData */

"use strict";

let graphs = require("./graphs");
let {Prefs} = require("./lib/prefs");

let {blocked} = self;

const IMPORTANT_NODE_ATTRIBUTES = ["style"];

function cleanupObject(object)
{
  let newObject = {};

  for (let attr in object)
  {
    if (object[attr])
      newObject[attr] = object[attr];
  }

  return newObject;
}

function domToGraph(element, isAd)
{
  let attributes = {};

  // Get all the attributes
  for (let attr of element.attributes)
    attributes[attr.name] = attr.value;

  for (let attr of IMPORTANT_NODE_ATTRIBUTES)
    attributes[attr] = cleanupObject(element[attr]);

  // Create the graph object
  let node = new graphs.GraphNode(element.tagName, attributes);

  // Check if it's an ad
  let details = blocked.get(element);
  if (details || isAd)
    node.ad = 1;

  // Check which filter it hit
  // eslint-disable-next-line no-warning-comments
  // TODO: Should the filter be passed recursively?
  if (details)
  {
    node.filter = details[0];
    node.requestType = details[1];
    if (node.filter.includes("##"))
      node.elementHidden = true;
    else
      node.elementBlocked = true;
  }

  // Add CSS selectors and size attributes
  node.cssSelectors = window.getComputedStyle(element).cssText;
  node.height = element.clientHeight;
  node.width = element.clientWidth;

  // DFS through the chilren
  for (let child of element.children)
    node.addChild(domToGraph(child, node.ad));
  return node;
}

/**
 * Represents an undirected graph. Used for producing adjacency and feature
 * matrices.
 */
class UndirectedGraph
{
  /**
   * Initialize an empty graph of a predefined size
   * @param {number} numOfVertices - size of a new graph
   * @private
   */
  constructor(numOfVertices)
  {
    // Create adjacency matrix and initialize it with '0's
    let emptyMatrix = new Array(numOfVertices);
    for (let i = 0; i < numOfVertices; i++)
      emptyMatrix[i] = new Array(numOfVertices).fill(0);
    this.adjacencyMatrix = emptyMatrix;
  }

  /**
   * Add an edge from node A to node B
   * @param {number} posA - number of a node from which to add an edge
   * @param {number} posB - number of a node into which to add an edge
   * @return {bool} whether operation was succesful
   * @private
   */
  addEdge(posA, posB)
  {
    if (posA < 0 || posB < 0)
      throw new Error("Can't add an Edge to vertex at negative position ");

    let numOfVertices = this.adjacencyMatrix.length;
    if (posA >= numOfVertices || posB >= numOfVertices)
      return false;
    this.adjacencyMatrix[posA][posB] = 1;
    // We want to be symmetric, as this is undirected graph
    this.adjacencyMatrix[posB][posA] = 1;
    return true;
  }
}

// Source: https://github.com/sindresorhus/html-tags/blob/master/html-tags.json
let htmlTags = [
  "a", "abbr", "address", "area", "article", "aside", "audio",
  "b", "base", "bdi", "bdo", "blockquote", "body", "br", "button", "canvas",
  "caption", "cite", "code", "col", "colgroup", "data", "datalist", "dd", "del",
  "details", "dfn", "dialog", "div", "dl", "dt", "em", "embed", "fieldset",
  "figcaption", "figure", "footer", "form", "h1", "h2", "h3", "h4", "h5", "h6",
  "head", "header", "hgroup", "hr", "html", "i", "iframe", "img", "input",
  "ins", "kbd", "keygen", "label", "legend", "li", "link", "main", "map",
  "mark", "math", "menu", "menuitem", "meta", "meter", "nav", "noscript",
  "object", "ol", "optgroup", "option", "output", "p", "param", "picture",
  "pre", "progress", "q", "rb", "rp", "rt", "rtc", "ruby", "s", "samp",
  "script", "section", "select", "slot", "small", "source", "span", "strong",
  "style", "sub", "summary", "sup", "svg", "table", "tbody", "td", "template",
  "textarea", "tfoot", "th", "thead", "time", "title", "tr", "track", "u",
  "ul", "var", "video", "wbr"
];
const GRAPH_CUT_OFF = 200;
const THRESHOLD = 0.33;
const HTML_TAGS_COUNT = 13;

/**
* A (tag, int) Map of all HTML tags with values reflecting the number of a tag
* in alphabetical enumeration.
* @type {Map}
*/
let htmlTagsObject = {
  "a": 7, "abbr": 2, "address": 2, "area": 5, "article": 10, "aside": 10,
  "audio": 6, "b": 2, "base": 11, "bdi": 2, "bdo": 2, "blockquote": 2,
  "body": 1, "br": 1, "button": 3, "canvas": 5, "caption": 9, "cite": 2,
  "code": 2, "col": 9, "colgroup": 9, "data": 10, "datalist": 3, "dd": 8,
  "del": 2, "details": 10, "dfn": 2, "dialog": 10, "div": 10, "dl": 8,
  "dt": 8, "em": 2, "embed": 12, "fieldset": 3, "figcaption": 5,
  "figure": 5, "footer": 10, "form": 3, "h1": 1, "h2": 1, "h3": 1, "h4": 1,
  "h5": 1, "h6": 1, "head": 1, "header": 10, "hgroup": 13, "hr": 1,
  "html": 1, "i": 2, "iframe": 4, "img": 5, "input": 3, "ins": 2, "kbd": 2,
  "keygen": 13, "label": 3, "legend": 3, "li": 8, "link": 7, "main": 10,
  "map": 5, "mark": 2, "math": 13, "menu": 13, "menuitem": 13, "meta": 11,
  "meter": 2, "nav": 7, "noscript": 12, "object": 12, "ol": 8,
  "opp.ex1 {displtgroup": 3, "option": 3, "output": 3, "p": 1, "param": 12,
  "picture": 5, "pre": 2, "progress": 2, "q": 2, "rb": 13, "rp": 2, "rt": 2,
  "rtc": 13, "ruby": 2, "s": 2, "samp": 2, "script": 12, "section": 10,
  "select": 3, "slot": 13, "small": 2, "source": 6, "span": 10, "strong": 2,
  "style": 10, "sub": 2, "summary": 10, "sup": 2, "svg": 5, "table": 9,
  "tbody": 9, "td": 9, "template": 2, "textarea": 3, "tfoot": 9, "th": 9,
  "thead": 9, "time": 2, "title": 1, "tr": 9, "track": 6, "u": 2, "ul": 8,
  "var": 2, "video": 6, "wbr": 2
};

const DEBUG_CSS_PROPERTIES_SNIPPETS = [
  ["background", "repeating-linear-gradient(to bottom, " +
                 "#e67370 0, #e67370 9px, white 9px, white 10px)"],
  ["outline", "solid red"]
];

let environment = {};
if (Prefs.elemhide_debug)
  environment.debugCSSProperties = DEBUG_CSS_PROPERTIES_SNIPPETS;

let htmlTagsMap = new Map(Object.entries(htmlTagsObject).map(
  entry => [entry[0], entry[1]]));

/**
 * Builds an adjecency matrix and a feature matrix based on an input element.
 * @param {Element} target Input element to convert.
 * @param {string} tagName An HTML tag name to filter mutations.
 * @returns {Tuple} (adjMatrix, elementTags) - a 2D array that represent an
 * adjacency matrix of an element. HTML elements are undirected trees, so the
 * adjacency matrix is symmetric.
 * elementTags - a 1D feature matrix, where each element is represents a type
 * of a node.
 * @private
 */
function processElement(target)
{
  let elementGraph = new UndirectedGraph(GRAPH_CUT_OFF);
  let numOfElements = 1;

  let elementTags = new Array(GRAPH_CUT_OFF).fill(0);

  let addEdges = (parentElement, parentPos) =>
  {
    let children = parentElement.children;
    for (let child of children)
    {
      if (numOfElements > GRAPH_CUT_OFF)
        break;

      elementTags[numOfElements] = htmlTagsMap.get(child.tagName) || 0;
      elementGraph.addEdge(parentPos, numOfElements);
      addEdges(child, numOfElements++);
    }
  };

  elementTags[0] = (htmlTagsMap.get(target.tagName) || 0);

  // Kick off recursive graph building
  addEdges(target, 0);
  let adjMatrix = elementGraph.adjacencyMatrix;
  return {adjMatrix, elementTags};
}

function recursivelyHideElements(target, predictions)
{
  let elementPosition = 0;

  let recursiveHideElement = parentElement =>
  {
    let children = parentElement.children;

    if (predictions[elementPosition + 1] > THRESHOLD ||
      predictions[elementPosition + 2] > THRESHOLD)
    {
      // console.log(parentElement);
      hideElement(parentElement);
    }

    for (let child of children)
    {
      if (elementPosition > GRAPH_CUT_OFF * 3)
        break;

      elementPosition += 3;
      recursiveHideElement(child);
    }
  };

  recursiveHideElement(target);

  // console.log("Traversed " + elementPosition + " nodes");
}

/**
 * Hides an HTML element by setting its `style` attribute to
 * `display: none !important`.
 *
 * @param {HTMLElement} element The HTML element to hide.
 * @private
 */

function hideElement(element)
{
  let {style} = element;
  let properties = [];

  for (let [key, value] of environment.debugCSSProperties ||
                           [["display", "none"]])
  {
    style.setProperty(key, value, "important");
    properties.push([key, style.getPropertyValue(key)]);
  }

  // Listen for changes to the style property and if our values are unset
  // then reset them.
  new MutationObserver(() =>
  {
    for (let [key, value] of properties)
    {
      if (style.getPropertyValue(key) != value ||
          style.getPropertyPriority(key) != "important")
        style.setProperty(key, value, "important");
    }
  }).observe(element, {attributes: true,
                       attributeFilter: ["style"]});
}

async function sendDataAndRunInference()
{
  for (let url of Prefs.mlListOfWebsites)
  {
    if (document.domain.indexOf(url) >= 0)
    {
      // Send new data to the server
      browser.runtime.sendMessage({
        type: "ml.dom",
        url: document.URL,
        isIFrame: window.top != window.self,
        graph: domToGraph(document.body, 0)
      });

      let processedElement = processElement(document.body);
      // Run inference on the current page
      browser.runtime.sendMessage({
        type: "ml.inference",
        inputs: [
          {
            data: [processedElement.elementTags], preprocess: [
              {funcName: "cast", args: "int32"},
              {funcName: "pad", args: GRAPH_CUT_OFF},
              {funcName: "oneHot", args: HTML_TAGS_COUNT},
              {funcName: "cast", args: "float32"}
            ]
          },
          {
            data: [processedElement.adjMatrix], preprocess: [
              {funcName: "pad", args: GRAPH_CUT_OFF},
              {funcName: "unstack"},
              {funcName: "localPooling"},
              {funcName: "stack"}
            ]
          }
        ],
        model: "mlHideIfNodeMatches"
      }).then(prediction =>
      {
        recursivelyHideElements(document.body, prediction);
      });
      break;
    }
  }
}


async function waitAndRun()
{
  await new Promise(resolve =>
  {
    if (document.readyState == "complete")

      resolve();

    else

      window.addEventListener("load", resolve);
  });
  // Just wait for _some_ elements to get there
  while (document.body.children.length == 0)
    await new Promise(resolve => { setTimeout(resolve, 1000); });

  await new Promise(resolve => { setTimeout(resolve, 1000); });

  sendDataAndRunInference();
}

waitAndRun();
