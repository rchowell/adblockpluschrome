"use strict";

class GraphNode
{
  constructor(tag, attributes)
  {
    this.tag = tag;
    this.attributes = attributes;
    this.children = [];
    this.ad = 0;
    this.elementHidden = false;
    this.elementBlocked = false;
    this.filter = null;
    this.requestType = null;
    this.height = 0;
    this.width = 0;
    this.cssSelectors = null;
  }

  addChild(newChild)
  {
    this.children.push(newChild);
  }
}

exports.GraphNode = GraphNode;
