/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @module */

"use strict";

const {port} = require("./messaging");
const {ML} = require("../adblockpluscore/lib/ml");

const tfCore = require("@tensorflow/tfjs-core");
const tfConverter = require("@tensorflow/tfjs-converter");
const tfBackendCpu = require("@tensorflow/tfjs-backend-cpu");
const tfBackendWasm = require("@tensorflow/tfjs-backend-wasm");
const tfBackendWebGL = require("@tensorflow/tfjs-backend-webgl");

let tf = {};

for (let object of [
  tfCore, tfConverter,
  tfBackendCpu, tfBackendWasm, tfBackendWebGL
])
{
  for (let property in object)
  {
    if (!Object.prototype.hasOwnProperty.call(tf, property))
      tf[property] = object[property];
  }
}

let mlByModel = new Map([
  ["mlHideIfGraphMatches", new ML(tf)],
  ["mlHideIfNodeMatches", new ML(tf)]
]);

for (let [key, value] of mlByModel)
  value.modelURL = browser.runtime.getURL(`data/${key}/model.json`);

/**
 * Returns the inference on a ML model.
 *
 * @event "ml.inference"
 * @property {string} model Name of the model to use
 * @returns {Array.<number>}
 */
port.on("ml.inference", (message, sender) =>
{
  let ml = mlByModel.get(message.model);
  if (!ml)
    return Promise.reject(new Error("Model not found."));

  return ml.predict(message.inputs);
});
